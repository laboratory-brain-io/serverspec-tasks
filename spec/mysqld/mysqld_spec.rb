require 'spec_helper'

describe service('mysqld') do
  it { should be_enabled }
  it { should be_running }
end

describe port(3306) do
  it { should be_listening }
end

describe file('/etc/my.cnf') do
  it { should contain('character-set-server=utf8').from(/^[mysqld]/).to(/^end/) }
  it { should be_mode 644 }
end
