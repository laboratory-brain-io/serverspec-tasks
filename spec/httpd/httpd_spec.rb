require 'spec_helper'

describe package('httpd'), :if => os[:family] == 'redhat' do
  it { should be_installed }
end

describe service('httpd'), :if => os[:family] == 'redhat' do
  it { should be_enabled }
  it { should be_running }
end

describe port(80) do
  it { should be_listening }
end

describe file('/etc/httpd/conf/httpd.conf') do
  it { should be_file }
  its(:content) { should match /DocumentRoot \"\/var\/www\/html\"/ }
  its(:content) { should match /ServerTokens Prod/ }
  its(:content) { should match /^\s*ServerName localhost/ }
  its(:content) { should match /^\s*ServerSignature Off/ }
  its(:content) { should match /^\s*ServerTokens Prod/ }
  its(:content) { should match /<Directory \/>.*Options.*-Indexes.*<\/Directory>$/m }
  its(:content) { should_not match /^\s*ScriptAlias \/cgi-bin\/ \"\/var\/www\/cgi-bin\/\"/ }
  its(:content) { should_not match /^\s*<Directory \"\/var\/www\/cgi-bin\">/m }
  its(:content) { should_not match /^\s*Alias \/icons\/ \"\/var\/www\/icons\/\"/ }
end

describe file('/etc/httpd/conf.d/welcome.conf') do
  it { should_not be_file }
end
