require 'rake'
require 'rspec/core/rake_task'
require 'yaml'

begin
  properties = YAML.load_file(ENV['CONFIG_FILE_PATH'])
rescue
  properties = YAML.load_file('config.yml')
end
 
desc "Run serverspec to all hosts"
task :spec => 'serverspec:all'
 
class ServerspecTask < RSpec::Core::RakeTask
  attr_accessor :target

  def spec_command
    cmd = super
    "env TARGET_HOST=#{target} #{cmd}"
  end
end

parents = properties.keys.select { |key| properties[key].has_key?(:children) }
children = []
parents.each do |parent|
  properties[parent][:children].each do |child|
    children << child
  end
end
singles = properties.keys.select { |key| !parents.include?(key) && !children.include?(key) }
done = []

namespace :serverspec do
  task :all => properties.keys.select { |key|
    parents.include?(key) || singles.include?(key)
  }.map { |x|
    'serverspec:' + x
  }

  properties.keys.each do |key|
    if properties[key].has_key?(:children)
      properties[key][:children].each do |child|
        done << child

        properties[child][:hostname].each do |host|
          desc "Run serverspec to #{child}: #{host}"
          RSpec::Core::RakeTask.new(key.to_sym) do |t|
            ENV['TARGET_HOST'] = host
            t.pattern = 'spec/{' + properties[child][:tests].join(',') + '}/*_spec.rb'
          end
        end
      end

    else
      desc "Run serverspec to #{key}"
      next if done.include?(key)

      properties[key][:hostname].each do |host|
        RSpec::Core::RakeTask.new(key.to_sym) do |t|
          unless done.include?(key)
            ENV['TARGET_HOST'] = host
            t.pattern = 'spec/{' + properties[key][:roles].join(',') + '}/*_spec.rb'
          end
        end
      end
    end
  end
end
